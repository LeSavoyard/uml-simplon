| Cas d’utilisation        | Soumettre Projet                                             |
| ------------------------ | ------------------------------------------------------------ |
| Référence (ID)           | 03                                                           |
| Acteur(s) Principal(aux) | Formateur                                                    |
| Acteur(s) Secondaire(s)  | --------------                                               |
| Date création            | 24/11/2020                                                   |
| Responsable              | Marques Lucas                                                |
| Pré-condition            | Être authentifier en tant que Formateur                      |
| Scénario Nominal         | 1. #formateur s'authentification<br />2. #formateur saisi le nom du projet<br />3. #formateur ajoute une description au projet<br />4. #formateur précise les livrables attendu<br />5. #formateur ajoute le référentiels<br />6. #formateur ajoute les sources<br />7. \#formateur associe le projet à une ou plusieurs promo<br />8. \#formateur valide le projet |
| Post-Condition           | La ou les Promos doivent éxister                             |
| Scénarios alternatifs    | --------------                                               |

