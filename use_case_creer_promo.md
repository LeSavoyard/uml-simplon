| cas d'utilisation     | Créer une nouvelle promo                                     |
| --------------------- | ------------------------------------------------------------ |
| id                    | 01                                                           |
| Acteur principal      | #Chargé de promo                                             |
| Acteurs secondaires   |                                                              |
| Responsable           |                                                              |
| Pré-condition         | L'utilisateur #chargé de promo a été créé<br />#chargé de promo s'est vu assigné un centre de formation<br />#chargé de promo est connecté |
| Scénario Nominal      | 1. #chargé de promo clique sur "mon centre de formation"<br />2. #chargé de promo clique sur "créer une nouvelle promotion"<br />3. un formulaire apparaît<br />4. #chargé de promo rempli le champ "nom de la promotion"<br />5. #chargé de promo rempli le champ "référentiels à valider" (une liste des référentiels)<br />6. #chargé de promo rempli le champ "assignation formateur(s)"<br />7. #chargé de promo remplit le champ "durée de la formation"<br />8. #chargé de promo remplit le champ "date de départ de la formation"<br />9. #chargé de promo remplit l'agenda de la formation<br />10. #chargé de promo envoie le formulaire |
| Post-condition        | Une promotion apparaît dans la liste des formations<br />La promotion est enregistrée en base de données |
| Scénarios alternatifs | A1<br />1. #chargé de promo n'affecte pas de formateur ou de durée et date de départ ou de référentiels à valider à la formation<br />2. La formation n'est pas visible par les visiteurs sur le site tant que ces champs ne sont pas remplis |

