## Use Case

### User

![](./img/use_case_user.png)

User is only able to connect and use school calendar, from there he can candidate.

### Student

![](./img/use_case_student.png)

Student is able to read a project created by a teacher.

While Student is reading project objectives he can submit his response.

### Teacher

![](./img/use_case_teacher.png)

Teacher submits projects for students to read, he approves student's skills through a project give back submission or directly in the student's profile.

### PromDirector

![](./img/use_case_prom_director.png)

Prom Director notifys late students and absences.

### RegionDirector

![](./img/use_case_region_director.png)

Region Director is pretty straight forward.

## Activity Diagram

### student submits project give back

![](./img/activity_diagram_give_back_submit.png)

What happens when a student submits a project give back.

Link to "consulter un rendu" on teacher's side where he's supposed to respond to this submission.

### confirm student's skills

![](./img/activity_diagram_give_back.png)

What happens when a teacher wants to confirm a student's skills within a project give back submission.

## Entity Diagram

